import Link from 'next/link';

export const HomePage = () => {
    return (
        <>
            <h1>Home Page</h1>
            <hr/>
            <div>
                <Link href="/details"><a>Go to Content</a></Link>
            </div>
            <div>
                <Link href="/posts"><a>Go to Posts</a></Link>
            </div>
        </>
    )
}
