# django-docker-template

## Note

- the order of `setdefault` method and `get_asgi_application` method in core/asgi.py is essential
- package `websockets` is needed for uvicorn to work with websocket connections

## docker-compose.yml

```yml
version: "3"

services: 
    web:
        build: .
        command: uvicorn core.asgi:application --host 0.0.0.0 --port 8000  # uvicorn対応
        environment: 
            APPLICATION_NAME: django-channels-private-chat-tutorial
            ENVIRONMENT: development
            SECRET_KEY:
            DEBUG: 1
            CLOUDINARY_NAME:
            CLOUDINARY_API_KEY:
            CLOUDINARY_API_SECRET:
        volumes: 
            - .:/app
        ports: 
            - 8000:8000
        depends_on:
            - redis
            - db
    redis:
        image: redis:6.0.9-alpine
        ports:
            - "6379:6379"
    db:
        image: postgres:11
        volumes:
            - postgres_data:/var/lib/postgresql/data/
        environment:
            - "POSTGRES_HOST_AUTH_METHOD=trust"

volumes:
    postgres_data:
```
