REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'dj_rest_auth.jwt_auth.JWTCookieAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (  # DRFの基本Renderを設定
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',  # 返されるJSONのキーをキャメルケースにする
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (  # DRFの基本Parserを設定
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',  # 返されるJSONのキーをキャメルケースにする
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
    'DEFAULT_PERMISSION_CLASSES': (  # DRFの基本パーミッションを設定
        'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    ),
    'DEFAULT_FILTER_BACKENDS': (  # DRFに検索機能とフィルタリング機能を追加
        'rest_framework.filters.SearchFilter',
        'rest_framework.filters.OrderingFilter',
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
    'SEARCH_PARAM': 'search',  # 検索文字を設定
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',  # テスト時のフォーマットをデフォルトでJSONに設定
}
