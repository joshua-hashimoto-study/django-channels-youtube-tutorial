REST_USE_JWT = True
JWT_AUTH_COOKIE = 'jwt-rest-auth'
REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'users.serializers.CustomUserDetailSerializer',
}
