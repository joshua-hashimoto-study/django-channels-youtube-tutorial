from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include


def api_url(base_url=''):
    api_base_url = 'api/v1'
    if not base_url:
        return api_base_url + '/'
    return f'{api_base_url}/{base_url}/'


urlpatterns = [
    # django admin
    path('headquarters/', admin.site.urls),
    # third party
    path('admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    path(api_url('auth'), include('dj_rest_auth.urls')),
    path(api_url('auth/register'), include('dj_rest_auth.registration.urls')),
    # local apps
    path(api_url(), include('chat.urls')),
    path('', include('frontend.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
