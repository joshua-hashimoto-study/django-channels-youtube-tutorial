from django.contrib import admin

from .forms import MessageAdminForm
from .models import ChatRoom, Message


class MessageInline(admin.TabularInline):
    model = Message
    extra = 1
    form = MessageAdminForm


class ChatRoomModelAdmin (admin.ModelAdmin):
    filter_horizontal = ('members',)
    readonly_fields = ('id', )
    fieldsets = (
        (None, {
            "fields": (
                'id',
                'room_name',
                'members',
                'chat_image',
                'is_group',
            ),
        }),
    )
    inlines = (MessageInline, )

    def get_list_display(self, request):
        """
        override get_list_display to get request object to
        get current logged in user.

        if you want to use the request object for custom
        methods you would make the custom method in get_list_display
        method and return the list containing the method
        """
        def is_mine(obj):
            return request.user in obj.members.all()
        is_mine.short_description = 'Room Joined'
        is_mine.boolean = True

        return ['id', 'room_name', is_mine]


class MessageModelAdmin(admin.ModelAdmin):
    form = MessageAdminForm
    readonly_fields = ('id', )
    fieldsets = (
        (None, {
            "fields": (
                'id',
                'chat_room',
                'user',
                'message',
                'is_active',
            ),
        }),
    )


admin.site.register(ChatRoom, ChatRoomModelAdmin)
admin.site.register(Message, MessageModelAdmin)
