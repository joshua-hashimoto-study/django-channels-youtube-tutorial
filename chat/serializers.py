from django.db.models import fields
from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import ChatRoom, Message


class MessageSerializer(ModelSerializer):

    class Meta:
        model = Message
        fields = (
            'id',
            'chat_room',
            'user',
            'message',
            'timestamp',
            'updated',
        )


class ChatRoomSerializer(ModelSerializer):
    chat_image = SerializerMethodField()

    class Meta:
        model = ChatRoom
        fields = (
            'id',
            'chat_image',
            'room_name',
            'members',
            'is_group',
            'timestamp',
            'updated',
        )

    def get_chat_image(self, obj):
        if (chat_image := obj.chat_image):
            return chat_image.url
        else:
            return None
