from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from .models import ChatRoom, Message
from .serializers import ChatRoomSerializer, MessageSerializer


class ChatRoomListAPIView(ListAPIView):
    serializer_class = ChatRoomSerializer
    queryset = ChatRoom.objects.all()
    permission_classes = (IsAuthenticated, )


class MessageListAPIView(ListAPIView):
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        queryset = Message.objects.all()
        if (chat_room_id := self.request.query_params.get('room')):
            try:
                queryset = queryset.filter(chat_room__id=chat_room_id)
            except:
                queryset = queryset.none()
        return queryset
