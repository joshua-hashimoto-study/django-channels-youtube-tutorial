import secrets

from cloudinary_storage.storage import MediaCloudinaryStorage
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q

from core.additionals.models import CoreModel


User = get_user_model()


class ChatRoomQuerySet(models.QuerySet):
    """
    """

    def all(self):
        """
        """
        queryset = self.filter(is_active=True)
        return queryset

    def search(self, query):
        """
        Args:
            query (str): search query
        """
        lookup = (
            Q(is_active=True) &
            Q(chat_room_name__icontains=query)
        )
        queryset = self.filter(lookup)
        return queryset


class ChatRoomManager(models.Manager):
    """
    """

    def get_queryset(self):
        """
        """
        return ChatRoomQuerySet(self.model, using=self._db)

    def all(self):
        """
        """
        queryset = self.get_queryset().all()
        return queryset

    def search(self, query):
        """
        """
        if query is None:
            return self.get_queryset().none()
        queryset = self.get_queryset().search(query)
        return queryset


def get_chat_room_default_name():
    return secrets.token_urlsafe(10)


def upload_chat_image_to(instance, filename):
    """
    custom path for saving images

    Returns:
        str: image path
    """
    asset_path = f'sample-chat/{str(instance.id)}/images/{filename}'
    return asset_path


class ChatRoom(CoreModel):
    """
    """
    room_name = models.CharField(
        max_length=250, blank=False, null=False, default=get_chat_room_default_name)
    members = models.ManyToManyField(
        User, related_name='chats', blank=True)
    chat_image = models.ImageField(
        upload_to=upload_chat_image_to, blank=True, null=True, storage=MediaCloudinaryStorage())
    is_group = models.BooleanField(default=False)

    objects = ChatRoomManager()

    def __str__(self):
        return self.room_name


class MessageQuerySet(models.QuerySet):
    """
    """

    def all(self):
        """
        """
        queryset = self.filter(is_active=True)
        return queryset

    def search(self, query):
        """
        Args:
            query (str): search query
        """
        lookup = (
            Q(is_active=True) &
            Q(message__icontains=query)
        )
        queryset = self.filter(lookup)
        return queryset


class MessageManager(models.Manager):
    """
    """

    def get_queryset(self):
        """
        """
        return MessageQuerySet(self.model, using=self._db)

    def all(self):
        """
        """
        queryset = self.get_queryset().all()
        return queryset

    def search(self, query):
        """
        """
        if query is None:
            return self.get_queryset().none()
        queryset = self.get_queryset().search(query)
        return queryset


class Message(CoreModel):
    """
    """
    chat_room = models.ForeignKey(
        ChatRoom, on_delete=models.CASCADE, related_name='messages')
    user = models.ForeignKey(
        User, related_name='messages', on_delete=models.CASCADE)
    message = models.TextField()

    objects = MessageManager()

    def __str__(self):
        return self.message
