import json
from typing import Dict
from uuid import uuid4

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer


class ChatConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        """
        """
        print('connecting...')
        self.room_id = self.scope.get('url_route').get('kwargs').get('room_id')
        await self.channel_layer.group_add(
            self.room_id,
            self.channel_name,
        )
        await self.accept()

    async def disconnect(self, code):
        """
        called when websocket has closed for any reason.
        """
        print(f'disconnect from room {self.room_id}...')
        await self.channel_layer.group_discard(
            self.room_id,
            self.channel_name,
        )

    async def receive_json(self, content: Dict, **kwargs):
        await self.channel_layer.group_send(
            self.room_id,
            {
                'type': 'send.message',
                'message': content.get('message', ''),
                'to': 'send_message'
            },
        )

    async def send_message(self, content: Dict):
        context = {
            'type': 'send_message',
            # 'user': user,
            'message': content.get('message'),
            'to': content.get('to'),
            'message_id': str(uuid4()),
        }
        try:
            user = self.scope.get('user', None)
            context.update({'user': str(user.id)})
        except:
            context.update({'user': None})
        await self.send_json(context)
        # await self.send(text_data=json.dumps(context))


# import json
# from typing import List

# from django.contrib.auth import get_user_model
# from asgiref.sync import async_to_sync
# from channels.generic.websocket import WebsocketConsumer

# from .models import Message


# class ChatConsumer(WebsocketConsumer):

#     def connect(self):
#         self.room_name = self.scope['url_route']['kwargs']['room_name']
#         self.room_group_name = 'chat_%s' % self.room_name
#         # Join room group
#         async_to_sync(self.channel_layer.group_add)(
#             self.room_group_name,
#             self.channel_name
#         )
#         self.accept()

#     def disconnect(self, close_code):
#         # Leave room group
#         async_to_sync(self.channel_layer.group_discard)(
#             self.room_group_name,
#             self.channel_name
#         )

#     # Receive message from WebSocket
#     def receive(self, text_data):
#         data = json.loads(text_data)
#         command = data['command']
#         if command == 'fetch_messages':
#             self.fetch_messages()
#         elif command == 'new_message':
#             self.new_message(data['message'])

#     def fetch_messages(self):
#         try:
#             # messages = Message.last_10_messages()
#             messages = Message.objects.all()
#             content = {
#                 'command': 'messages',
#                 'messages': self.messages_to_json(messages)
#             }
#             self.send_message(content)
#         except Exception as err:
#             print(err)
#             print('fetch_message error')

#     def new_message(self, data):
#         try:
#             author_id = data['from']
#             author_user = get_user_model().objects.get(id=author_id)
#             message: Message = Message.objects.create(
#                 author=author_user, message=data['message'])
#             content = {
#                 'command': 'new_message',
#                 'message': message.to_dict(),
#             }
#             return self.send_chat_message(content)
#         except:
#             print('new_message error')

#     def messages_to_json(self, messages: List[Message]):
#         result = []
#         for message in messages:
#             result.append(message.to_dict())
#         return result

#     def send_chat_message(self, message):
#         # Send message to room group
#         async_to_sync(self.channel_layer.group_send)(
#             self.room_group_name,
#             {
#                 'type': 'chat_message',
#                 'message': message
#             }
#         )

#     def send_message(self, message):
#         self.send(text_data=json.dumps(message))

#     def chat_message(self, message):
#         print(message)
#         message = message['message']
#         self.send(text_data=json.dumps(message))
