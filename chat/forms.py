from django import forms
from django.contrib.auth import get_user_model

from .models import Message, ChatRoom


User = get_user_model()


class MessageAdminForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = Message
        fields = (
            'chat_room',
            'user',
            'message',
        )

    def clean_message(self, *args, **kwargs):
        chat_room = self.cleaned_data.get('chat_room')
        user = self.cleaned_data.get('user')
        if user not in chat_room.members.all():
            raise forms.ValidationError('User is not in the chat room.')
        message = self.cleaned_data.get('message')
        return message
