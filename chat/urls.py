from django.urls import path

from .views import ChatRoomListAPIView, MessageListAPIView

app_name = 'chats'

urlpatterns = [
    path('chatrooms/', ChatRoomListAPIView.as_view(), name='chatroom_list'),
    path('messages/', MessageListAPIView.as_view(), name='message_list'),
]
